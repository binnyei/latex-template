#target: dependency
#	command

#First line is the default when make is executed
#Phony: target that does not build anything
#first target is the default one



path_to_diagrams="images/diagrams"
path_to_latex_files="latex_files"
file_name=main

.PHONY: build_pdftex
build_pdftex:
	docker run -v $(PWD):/latex_project/ latex pdflatex $(file_name).tex
	docker run -v $(PWD):/latex_project/ latex biber $(file_name)
	docker run -v $(PWD):/latex_project/ latex pdflatex $(file_name).tex

.PHONY: clean
clean:
	rm -f $(path_to_latex_files)/*.aux
	rm -f $(path_to_latex_files)/*.log
	rm -f $(path_to_latex_files)/*.toc
	rm -f $(path_to_latex_files)/*.lof
	rm -f $(path_to_latex_files)/*.lol
	rm -f $(path_to_latex_files)/*.lot
	rm -f $(path_to_latex_files)/*.fls
	rm -f $(path_to_latex_files)/*.out
	rm -f $(path_to_latex_files)/*.fdb_latexmk
	rm -f $(path_to_latex_files)/*.auto
	rm -f $(path_to_latex_files)/*.bbl
	rm -f $(path_to_latex_files)/*.bib.blg
	rm -f $(path_to_latex_files)/*.blg
	rm -f $(path_to_latex_files)/*.bcf
	rm -f $(path_to_latex_files)/*.dvi
	rm -f $(path_to_latex_files)/*.out.ps
	rm -f $(path_to_latex_files)/*.run.xml
	rm -f $(path_to_latex_files)/*.pdf

uml:
	docker run -v $(PWD)/$(path_to_diagrams):/plantuml/ plantuml java -jar /plantuml.jar /plantuml

docker_setup:
	docker build -t latex -f docker/Dockerfile .
	docker build -t plantuml -f docker/Plant_UML_Dockerfile . 

docker_clean:
	docker system prune
	docker rmi latex
	docker rmi plantuml
	docker system prune