# LaTex template to go (with Docker)
## Goal:
As I have started to write a lot of documentations and other texts with LaTex, I wanted to have a template where I can start writing without having to install or set up a lot.
Therefore I have created this repo with a base template with examples on how to use what (tables, figures, ...).

## Usage:
* Switch to directory **latex_files** 
* * Update: Makefile added in root folder to be in same directory as git
* Use command **make docker_setup** for container creation (prerequisite: installed Docker application)
* After container is initialized use command **make** for creating a PDF file
* * Name of file can be edited in Makefile
* Command **make clean** deletes all temporary files that were created from LaTex
* * Cleaning up is why the Makefile is located in this directory
* Command **make docker_clean** removes LaTex container and image

## PlantUML:
PlantUML is a tool that allows you to create UML and other diagrams from a textfile. You can create diagrams from files stored in images/diagrams by using the rule **make uml**.
More info to PLantUML can be found here: https://plantuml.com

## Todo:
- [ ] Play with Python code highlighting (not working at the moment)
- [x] Find smaller Texlive version (and install missing packages) => reduced from 3.5GB to 900MB