@startuml
!include https://raw.githubusercontent.com/bschwarz/puml-themes/master/themes/cerulean/puml-theme-cerulean.puml
start
:Import photos;
:Rate photos with stars;
switch (Ratings)
    case (one or two stars)
        :Good enough photos
        for sharing
        just store them;
        stop
    case (three stars)
        :Good images
        where editing
        can bring out
        even better quality;
    case (four stars)
        :Photos will
        be taken for
        photo books
        or printouts;
    case (five stars)
        :Photos will
        be used for
        portfolio,
        photo of the month
         or contests;
endswitch
:Edit photos in LightRoom or Photoshop;
stop
@enduml